# Mutation Seq WGS Pipeline on Azure

### Development Resource Group

* Resource group: pascale01
* Batch account: pascale
* Storage account: pascale

* Batch account: control pools
* Storage account: blob storage
  * Containers for reference data, images and startup resources
  * In the startup-resources-museq container, startup.sh is run every time a compute node is started
  * From the headnode, more disks can be mounted

 ### Mounting Disks on Head Node

 (Adapted from [https://help.ubuntu.com/community/InstallingANewHardDrive](https://help.ubuntu.com/community/InstallingANewHardDrive))

* From the headnode on portal.azure.com, under Settings, click Disks
* Click Add data disk
* In the dropdown under Name, click Create disk and fill out the form as desired
* ssh into the headnode in a terminal (use the IP address under Public IP address in the Essentials tray at the top of the Overview page)
* Run `sudo lshw –C disk`
  * Note the logical name of the unmounted disk. It should be something like `/dev/sdb`
* To partition the disk, run `sudo fdisk <logical name>`
  * Type `n <enter>` to add a new partition
  * Type `p <enter>` to get a primary partition
  * Type `1 <enter>` since this will be the only partition on the device
  * Press `<enter>` twice when asked about the size of the disk
  * Type `w <enter>` to write the partition table to the disk
  * The name of the partition should now be `<logical name>1`, while the disk is named `<logical name>`. For example, if the disk name is `/dev/sdb`, the partition would be named `/dev/sdb1`
* To format the disk, run `sudo mkfs –t ext3 <partition name>`
* To access the drive, you will need to create a mount point. Run `sudo mkdir /media/mynewdrive`, using a directory name that is useful
* To mount the drive, run `sudo mount <partition name> <path to mount point>`

### Museq Pipeline on Azure Batch

(Adapted from [https://svn.bcgsc.ca/bitbucket/users/dgrewal/repos/azure-sc/browse](https://svn.bcgsc.ca/bitbucket/users/dgrewal/repos/azure-sc/browse))

* Start a batch service from Azure portal (also creates new resource group and storage account)
* Install Azure CLI on your local machine (use brew for macOS)
```
brew update && brew install azure-cli
```
* Create a new VM to run everything from
```
az vm create --resource-group batchdg --name headnode --image UbuntuLTS --generate-ssh-keys
```
* Log into the VM
  * The headnode’s IP can be found on the Azure portal under Public IP address in the Essentials tray at the top of the Overview page
* Run the following:
```
sudo apt-get update && sudo apt-get install -y libssl-dev libffi-dev python-dev build-essential
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ wheezy main" | sudo tee /etc/apt/sources.list.d/azure-cli.list
sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893
sudo apt-get install apt-transport-https
sudo apt-get update && sudo apt-get install azure-cli

wget https://github.com/singularityware/singularity/releases/download/2.4/singularity-2.4.tar.gz
tar xvf singularity-2.4.tar.gz
cd singularity-2.4/
./configure --prefix=/usr/local
make
sudo make install

git clone https://dgrewal@bitbucket.org/dranew/pypeliner.git
git clone https://pwalters@svn.bcgsc.ca/bitbucket/scm/sc/museq_wgs_pypeliner.git
git clone https://pwalters@svn.bcgsc.ca/bitbucket/scm/museq/mutationseq.git
cp -r mutationseq/museq/ .

wget https://sourceforge.net/projects/boost/files/boost/1.57.0/boost_1_57_0.tar.gz
tar -xvf boost_1_57_0.tar.gz
```

* Build a new singularity image
  * Save the following to museq.recipe:

```
Bootstrap: docker
From: ubuntu:latest
%setup
    cp -r museq_wgs_pypeliner/ /tmp/
    cp -r pypeliner /tmp/
    cp -r museq /tmp/
    cp -r boost_1_57_0 /tmp/
%environment
    export PATH=/usr/local/miniconda2/bin:$PATH
%post
    apt-get update && apt-get -y install bzip2 wget libkeyutils-dev ssh ttf-dejavu fontconfig vim make build-essential libpng-dev zlib1g-dev
    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh -b -p /usr/local/miniconda2
    conda upgrade conda
    conda install --file /tmp/museq_wgs_pypeliner/conda_packages.txt -c bioconda -c anaconda
    pip install azure-storage azure-batch futures
    cd /tmp/pypeliner; python setup.py install
    mv /tmp/museq /usr/local/museq
    cd /usr/local/museq/
    make clean
    make PYTHON=python BOOSTPATH=/tmp/boost_1_57_0/
    pip install numpy
    pip install scikit-learn==0.13.1
    cd /tmp/museq_wgs_pypeliner; python setup.py install
```

  * Save the following to conda_packages.txt:

```
scikit-learn
intervaltree
numpy
scipy
matplotlib
pandas
pysam
pyyaml
networkx
dill
snpeff
museqportrait
```

  * Run `sudo singularity build mutation_seq.img museq.recipe`. This builds the singularity image in the .img file using the specifications in the .recipe file
    * During development, run `sudo singularity build --writable mutation_seq.img museq.recipe`. This builds a singularity image that can be modified. Run `sudo singularity shell --writable mutation_seq.img` to start a shell inside the singularity image. This is especially useful to ensure that all packages have been properly installed and files are in the correct places. More information can be found [here](http://singularity.lbl.gov/docs-build-container#--writable).

* Save the following to set-credentials.sh:

```
export AZURE_STORAGE_ACCOUNT='pascale'
export AZURE_STORAGE_KEY='{access key to storage account}'
export AZURE_BATCH_ACCOUNT='pascale'
export AZURE_BATCH_URL='https://pascale.canadacentral.batch.azure.com'
export AZURE_BATCH_KEY='/4yU0HIrRolOTir4VeGpc776/QM6XwUunZBirhQ24HeWIiJe6xiKbcpqLSxPc5Cnsqex1992qZW7BMSKKyPt1Q=='
export AZURE_REF_STORAGE_ACCOUNT='pascale'
export AZURE_REF_STORAGE_KEY='{access key to storage account}'
```

* Run `source set-credentials.sh`

### Setting Up the Storage Account

On the Azure portal, the storage account has the following containers for blob storage:

1. reference-grch37-museq: contains reference files for runs aligned to the human genome
2. reference-mm10-museq: contains reference files for runs aligned to the mouse genome
3. singularity-images-museq: contains singularity images
4. startup-resources-museq: contains startup.sh
  * **Important!** This script is run on every compute node when it starts. It can also be found in museq\_wgs\_pypeliner/azure/startup.sh
5. tasks-container-museq
6. results-museq: container to which results are written
7. logs-museq
8. temp-museq
9. data-museq: container for the bam files to be processed

### Running mutationseq

* Upload the singularity image to blob storage
```
az storage blob upload -c singularity-images-museq -n mutation_seq.img -f mutation_seq.img
```

* Run the pipeline with museq\_wgs\_pypeliner/azure/run.sh, which takes in the following parameters:
  1. Path to input CSV containing the paths to the bam files
  2. Jira ticket ID
  3. Reference genome (grch37 or mm10)
  4. Name of config file (museq.yaml)
  5. Extra arguments for pypeliner

* Results are written to the results-museq container, after which they are downloaded to /results/ on the head node



