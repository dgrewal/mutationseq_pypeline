'''
Created on Feb 21, 2018

@author: pwalters
'''
import pypeliner
import pypeliner.managed as mgd
import tasks


def create_museq_workflow(
        snv_vcf,
        snv_tsv,
        museqportrait_pdf,
        museqportrait_txt,
        config,
        tumour_bam=None,
        tumour_bai=None,
        normal_bam=None,
        normal_bai=None):

    workflow = pypeliner.workflow.Workflow()

    if tumour_bam and not normal_bam:
        # Unpaired with only tumour
        workflow.transform(
            name='run_museq_unpaired_tumour',
            ctx={
                'mem': config['memory']['high'],
                'pool_id': config['pools']['multicore'],
                'ncpus': config['threads'],
                'walltime': '08:00'},
            func=tasks.run_museq,
            args=(
                mgd.TempOutputFile('museq.vcf'),
                mgd.TempOutputFile('museq.log'),
                config,
            ),
            kwargs={
                'tumour_bam': mgd.InputFile(tumour_bam),
                'tumour_bai': mgd.InputFile(tumour_bai),
            }
        )
    elif not tumour_bam and normal_bam:
        # Unpaired with only normal
        workflow.transform(
            name='run_museq_unpaired_normal',
            ctx={
                'mem': config['memory']['high'],
                'pool_id': config['pools']['multicore'],
                'ncpus': config['threads'],
                'walltime': '08:00'},
            func=tasks.run_museq,
            args=(
                mgd.TempOutputFile('museq.vcf'),
                mgd.TempOutputFile('museq.log'),
                config,
            ),
            kwargs={
                'normal_bam': mgd.InputFile(normal_bam),
                'normal_bai': mgd.InputFile(normal_bai),
            }
        )
    else:
        # Paired
        workflow.transform(
            name='run_museq_paired',
            ctx={
                'mem': config['memory']['high'],
                'pool_id': config['pools']['multicore'],
                'ncpus': config['threads'],
                'walltime': '08:00'},
            func=tasks.run_museq,
            args=(
                mgd.TempOutputFile('museq.vcf'),
                mgd.TempOutputFile('museq.log'),
                config,
            ),
            kwargs={
                'tumour_bam': mgd.InputFile(tumour_bam),
                'tumour_bai': mgd.InputFile(tumour_bai),
                'normal_bam': mgd.InputFile(normal_bam),
                'normal_bai': mgd.InputFile(normal_bai),
            }
        )

    workflow.transform(
        name='run_snpeff',
        ctx={
            'mem': config['memory']['high'],
            'pool_id': config['pools']['highmem'],
            'ncpus': 1,'walltime': '01:00'},
        func=tasks.run_snpeff,
        args=(
            mgd.TempInputFile('museq.vcf'),
            mgd.TempOutputFile('museq_annotSnpEff.vcf'),
            config,
        ),
    )

    workflow.transform(
        name='run_mutation_assessor',
        ctx={
            'mem': config['memory']['low'],
            'pool_id': config['pools']['standard'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.run_mutation_assessor,
        args=(
            mgd.TempInputFile('museq_annotSnpEff.vcf'),
            mgd.TempOutputFile('museq_annotMA.vcf'),
            config,
        ),
    )

    workflow.transform(
        name='run_DBSNP',
        ctx={
            'mem': config['memory']['high'],
            'pool_id': config['pools']['highmem'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.run_DBSNP,
        args=(
            mgd.TempInputFile('museq_annotMA.vcf'),
            mgd.TempOutputFile('museq_flagDBsnp.vcf'),
            config,
        ),
    )

    workflow.transform(
        name='run_1000gen',
        ctx={
            'mem': config['memory']['high'],
            'pool_id': config['pools']['highmem'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.run_1000gen,
        args=(
            mgd.TempInputFile('museq_flagDBsnp.vcf'),
            mgd.TempOutputFile('museq_flag1000gen.vcf'),
            config,
        ),
    )

    workflow.transform(
        name='run_cosmic',
        ctx={
            'mem': config['memory']['high'],
            'pool_id': config['pools']['highmem'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.run_cosmic,
        args=(
            mgd.TempInputFile('museq_flag1000gen.vcf'),
            mgd.OutputFile(snv_vcf),
            config,
        ),
    )

    workflow.transform(
        name='run_museqportrait',
        ctx={
            'mem': config['memory']['low'],
            'pool_id': config['pools']['standard'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.run_museqportrait,
        args=(
            mgd.InputFile(snv_vcf),
            mgd.OutputFile(museqportrait_pdf),
            mgd.OutputFile(museqportrait_txt),
            mgd.TempOutputFile('museqportrait.log'),
        ),
    )

    workflow.transform(
        name='parse_museq',
        ctx={
            'mem': config['memory']['low'],
            'pool_id': config['pools']['standard'],
            'ncpus': 1, 'walltime': '01:00'},
        func=tasks.parse_museq,
        args=(
            mgd.InputFile(snv_vcf),
            mgd.OutputFile(snv_tsv),
            mgd.TempOutputFile('museq_low-map-filt.tsv')
        )
    )

    return workflow
