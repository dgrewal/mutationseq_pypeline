'''
Created on Feb 21, 2018

@author: pwalters
'''
import os
import pypeliner
import multiprocessing
from pypeliner_utils import helpers, vcfutils


scripts_directory = os.path.join(
    os.path.realpath(os.path.dirname(__file__)), 'scripts')


def _run_museq_worker(tumour_bam, normal_bam, out, log, config, chrom):
    '''
    Run museq script for each chromosome

    :param tumour: path to tumour bam
    :param normal: path to normal bam
    :param out: path to temporary output VCF file for the chromosome
    :param log: path to the log file
    :param config: path to the config YAML file
    :param chrom: chromosome number
    '''

    script = os.path.join(config['museq_params']['mutationseq'], 'classify.py')
    conf = os.path.join(
        config['museq_params']['mutationseq'], 'metadata.config')
    model = config['museq_params']['mutationseq_model']
    reference = config['museq_params']['reference_genome']

    cmd = ['python', script]

    if tumour_bam:
        cmd.append('tumour:' + tumour_bam)
    if normal_bam:
        cmd.append('normal:' + normal_bam)

    cmd.extend(['reference:' + reference, 'model:' + model, '--out', out,
                '--log', log, '--config', conf, '--interval', chrom, '-v'])

    helpers.run_cmd(cmd)


def run_museq(out, log, config, tumour_bam=None, tumour_bai=None,
              normal_bam=None, normal_bai=None):
    '''
    Run museq script for all chromosomes and merge VCF files

    :param tumour: path to tumour bam
    :param normal: path to normal bam
    :param out: path to the temporary output VCF file for the merged VCF files
    :param log: path to the log file
    :param config: path to the config YAML file
    '''

    count = config.get("threads", multiprocessing.cpu_count())

    pool = multiprocessing.Pool(processes=count)

    output_vcf = {}
    chroms = config['chromosomes']

    tasks = []

    for chrom in chroms:
        outfile = os.path.join(
            os.path.dirname(out), 'chr' + str(chrom) + '.vcf.tmp')

        task = pool.apply_async(
            _run_museq_worker,
            args=(
                tumour_bam,
                normal_bam,
                outfile,
                log,
                config,
                chrom,
            )
        )

        tasks.append(task)
        output_vcf[chrom] = outfile

    pool.close()
    pool.join()

    [task.get() for task in tasks]

    vcfutils.concatenate_vcf(output_vcf, out)


def run_snpeff(infile, output, config):
    '''
    Run snpEff script on the input VCF file

    :param infile: temporary input VCF file
    :param output: temporary output VCF file
    :param config: path to the config YAML file
    '''

    snpeff_config = config['snpeff_params']['snpeff_config']

    cmd = ['snpEff', '-Xmx5G', '-Xms5G', '-XX:ParallelGCThreads=1',
           '-c', snpeff_config, 'GRCh37.75', '-noStats', infile, '>',
           output]

    pypeliner.commandline.execute(*cmd)


def run_mutation_assessor(infile, output, config):
    '''
    Run Mutation Assessor script on the input VCF file

    :param infile: temporary input VCF file
    :param output: temporary output VCF file
    '''

    mutation_assessor_script = os.path.join(
        scripts_directory, 'annotate_mutation_assessor.py')
    db = config['mutation_assessor_params']['db']

    cmd = ['python', mutation_assessor_script, '--vcf', infile, '--output',
           output, '--db', db]

    pypeliner.commandline.execute(*cmd)


def run_DBSNP(infile, output, config):
    '''
    Run DBSNP script on the input VCF file

    :param infile: temporary input VCF file
    :param output: temporary output VCF file
    '''

    script = os.path.join(scripts_directory, 'flagPos.py')
    db = config['dbsnp_params']['db']

    cmd = ['python', script, '--infile', infile, '--db', db, '--out', output,
           '--label', 'DBSNP', '--input_type', 'snv', '--flag_with_id', 'True']

    pypeliner.commandline.execute(*cmd)


def run_1000gen(infile, output, config):
    '''
    Run 1000Gen script on the input VCF file

    :param infile: temporary input VCF file
    :param output: temporary output VCF file
    '''

    script = os.path.join(scripts_directory, 'flagPos.py')
    db = config['1000gen_params']['db']

    cmd = ['python', script, '--infile', infile, '--db', db, '--out', output,
           '--label', '1000Gen', '--input_type', 'snv']

    pypeliner.commandline.execute(*cmd)


def run_cosmic(infile, output, config):
    '''
    Run Cosmic script on the input VCF file

    :param infile: temporary input VCF file
    :param output: temporary output VCF file
    '''

    script = os.path.join(scripts_directory, 'flagPos.py')
    db = config['cosmic_params']['db']

    cmd = ['python', script, '--infile', infile, '--db', db, '--out', output,
           '--label', 'Cosmic', '--input_type', 'snv', '--flag_with_id',
           'True']

    pypeliner.commandline.execute(*cmd)


def run_museqportrait(infile, out_pdf, out_txt, museqportrait_log):
    '''
    Run museqportrait script on the input VCF file

    :param infile: temporary input VCF file
    :param out_dir: temporary output VCF file
    :param museqportrait_log: path to the log file
    '''

    cmd = ['museqportrait', '--log', museqportrait_log, '--output-pdf',
           out_pdf, '--output-txt', out_txt, infile]

    pypeliner.commandline.execute(*cmd)


def parse_museq(infile, output, low_map_filt_output):
    '''
    Parse the input VCF file into a TSV file

    :param infile: temporary input VCF file
    :param output: path to the output TSV file
    '''

    cmd = ['vizutils_parse_museq', '--infile', infile,
           '--pre_mappability_output', output,
           '--output', low_map_filt_output,
           '--keep_dbsnp', '--keep_1000gen', '--remove_duplicates']
    pypeliner.commandline.execute(*cmd)
