import os
import argparse
import utils
import pypeliner
import pypeliner.managed as mgd
from workflows import mutationseq


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    pypeliner.app.add_arguments(parser)

    parser.add_argument('--bams_file',
                        required=True,
                        help='''Path to input bam CSV.''')
    parser.add_argument('--config_file',
                        required=True,
                        help='''Path to yaml config file.''')
    parser.add_argument('--out_dir',
                        required=True,
                        help='''Path to output files.''')

    args = vars(parser.parse_args())

    return args


def make_bam_arg(arg_name, files):
    if files:
        bam_arg = mgd.InputFile(
            arg_name, 'sample_id', fnames=files, axes_origin=[])
    else:
        bam_arg = None
    return bam_arg


def main():
    args = parse_args()
    config = utils.load_config(args)

    pyp = pypeliner.app.Pypeline(config=args)

    tumour_bam, normal_bam, sample_ids = utils.read_bams_file(
        args['bams_file'])

    workflow = pypeliner.workflow.Workflow()
    workflow.setobj(obj=mgd.OutputChunks('sample_id'), value=tumour_bam.keys())

    if tumour_bam:
        tumour_bai = dict([(sample_id, str(tumour_bam[sample_id]) + '.bai')
                          for sample_id in sample_ids])
    if normal_bam:
        normal_bai = dict([(sample_id, str(normal_bam[sample_id]) + '.bai')
                          for sample_id in sample_ids])

    output_dir = os.path.join(args['out_dir'], '{sample_id}')
    snv_vcf = os.path.join(output_dir, 'museq.vcf')
    snv_tsv = os.path.join(output_dir, 'museq.tsv')
    museqportrait_pdf = os.path.join(output_dir, 'museqportrait.pdf')
    museqportrait_txt = os.path.join(output_dir, 'museqportrait.txt')

    args = (
        mgd.OutputFile(snv_vcf, 'sample_id'),
        mgd.OutputFile(snv_tsv, 'sample_id'),
        mgd.OutputFile(museqportrait_pdf, 'sample_id'),
        mgd.OutputFile(museqportrait_txt, 'sample_id'),
        config
    )
    kwargs = {
        'tumour_bam': make_bam_arg('tumour_bam', tumour_bam),
        'tumour_bai': make_bam_arg('tumour_bai', tumour_bai),
        'normal_bam': make_bam_arg('normal_bam', normal_bam),
        'normal_bai': make_bam_arg('normal_bai', normal_bai)
    }

    # Unpaired with only tumour
    if tumour_bam and not normal_bam:
        workflow_name = 'museq_unpaired_tumour'
    # Unpaired with only normal
    elif not tumour_bam and normal_bam:
        workflow_name = 'museq_unpaired_normal'
    # Paired
    elif tumour_bam and normal_bam:
        workflow_name = 'museq_paired'
    else:
        raise Exception('Input csv cannot be empty')

    workflow.subworkflow(
        name=workflow_name,
        func=mutationseq.create_museq_workflow,
        axes=('sample_id',),
        args=args,
        kwargs=kwargs
    )
    pyp.run(workflow)


if __name__ == '__main__':
    main()
